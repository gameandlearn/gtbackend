const express = require('express');
const session = require('express-session');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const sequelize = require('./config/database');
const cors = require('cors');
require('dotenv').config();
const authRoutes = require('./routes/auth.routes');
const monsterRoutes = require('./routes/monster.routes');
const userRoutes = require('./routes/user.routes');

// Importer le fichier de synchronisation des modèles
require('./models/index');

const app = express();
const port = process.env.PORT || 5000;

// Configurer la session
const sessionStore = new SequelizeStore({
    db: sequelize,
});

app.use(session({
    secret: process.env.SESSION_SECRET || 'mySecretKey', // Utilise une clé secrète plus sécurisée en production
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000, // 24 heures
    }
}));
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Route de test
app.get('/', (req, res) => {
    res.send('API is running...');
});
// Utiliser les routes
app.use('/api/auth', authRoutes);
app.use('/api', monsterRoutes);
app.use('/api', userRoutes);

// Fonction pour démarrer le serveur
const startServer = async () => {
    try {
        // Synchroniser le store de session
        await sessionStore.sync();
        console.log('strtsrv Session store synchronized');

        // Démarrer le serveur
        app.listen(port, () => {
            console.log(`strtsrv Server is running on port ${port}`);
        });
    } catch (error) {
        console.error('strtsrv Unable to start the server:', error);
    }
};

// Appeler la fonction pour démarrer le serveur
startServer();
