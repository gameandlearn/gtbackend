const sequelize = require('../config/database');
const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const AuthLogic = require('./logic/auth.logic');

// Inscription
exports.register = async (req, res) => {

    try {
        const { username, password } = req.body;
        verifiedUsername = AuthLogic.vérifyUsername(username)
        verifiedPassword = AuthLogic.vérifyPassword(password)

        if(!verifiedUsername.isCorrect || !verifiedPassword.isCorrect) {
            return res.status(400).json({message: "mot de passe ou nom invalide"})
        }

        // créer le user
        const user = await User.create({ username, password });
        req.session.userId = user.id; // Connecte automatiquement l'utilisateur après inscription
        //réponse
        res.status(201).json(user);

        
    } catch (error) {
        // retour erreur
        res.status(400).json({ error: error.message });
    }
};

// Connexion
exports.login = async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ where: { username } });

        if (!user || !(await bcrypt.compare(password, user.password))) {
            return res.status(401).json({ message: 'Invalid credentials' });
        }

        req.session.userId = user.id; // Stocke l'ID utilisateur dans la session
        res.status(200).json({ message: 'Logged in successfully', id: user.id, username: user.username });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

// Connexion
exports.loginIfSessionActive = async (req, res) => {
    try {
        const userId = req.params.id;
        console.log(userId)
        req.session.userId = userId; // Stocke l'ID utilisateur dans la session
        res.status(200).json({ message: 'Logged in successfully', id: userId, isLogged: true });
    } catch (error) {
        res.status(400).json({ error: error.message, isLogged: false });
    }
};

// Déconnexion
exports.logout = (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return res.status(500).json({ error: 'Could not log out, please try again' });
        }
        res.status(200).json({ message: 'Logged out successfully' });
    });
};
