const MonsterLogic = {}

// génère aléatoirement la rareté en fonction de la stone visée
MonsterLogic.getRarityByStones = (req) => {
    //random sur 1000 soit (1 = 0.1%)(10 = 1%)
    let rand = Math.floor(Math.random() * 1000);
    let stone = req.body.stone

    switch (stone) {
        case "basic_stones":
            if(rand <= 750){
                // 75%
                return {id: 1, name: "common"}
            }
            else if(rand > 750 && rand <= 970){
                // 22%
                return {id: 2, name: "rare"}
            }
            else if(rand > 970){
                // 3%
                return {id: 3, name: "epic"}
            }
            break;

        case "quality_stones":
            if(rand <= 800){
                // 80%
                return {id: 2, name: "rare"}
            }
            else if(rand > 800 && rand <= 970){
                // 17%
                return {id: 3, name: "epic"}
            }
            else if(rand > 970){
                // 3%
                return {id: 4, name: "legendary"}
            }
            break;

        case "scarlet_stones":
            if(rand <= 770){
                // 77%
                return {id: 3, name: "epic"}
            }
            else if(rand > 770 && rand <= 970){
                // 20%
                return {id: 4, name: "legendary"}
            }
            else if(rand > 970){
                // 3%
                return {id: 5, name: "mythic"}
            }
            break;
    
        default:
            break;
    }

}

module.exports = MonsterLogic