const AuthLogic = {}

// retourne un booléen pour attester que le username respecte certaine règles
// 4 < username < 15
AuthLogic.vérifyUsername = (username) => {
    if(username.length >= 4 && username.length < 15 ) {
        return {
            isCorrect: true,
            username: username
        }
    }
    return {
        isCorrect: false,
        message: "username must contain between 5 < 15 char"
    }
}

// retourne un booléen pour attester que le password respecte certaine règles
// 8 < password < 12 => .{8,12}
// contiens 1 ou pluieurs lettres => (?=.*[a-z])
// contiens 1 ou pluieurs capitales => (?=.*[A-Z])
// contiens 1 ou pluieurs charactère spéciale => (?=.*[\W_])
AuthLogic.vérifyPassword = (password) => {

    const regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_]).{8,12}$/;

    if(regexPassword.test(password)) {
        return {
            isCorrect: true,
            password: password
        }
    }
    return {
        isCorrect: false,
        message: "username must contain between 5 < 15 char"
    }
}

module.exports = AuthLogic