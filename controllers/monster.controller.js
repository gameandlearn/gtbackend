const { Op } = require('sequelize');
const Monster = require('../models/monster.model');
const OriginalMonster = require('../models/originalMonster.model');
const Rarity = require('../models/rarity.model');
const Role = require('../models/role.model');
const Type = require('../models/type.model');
const MonsterLogic = require('./logic/monster.logic');


exports.summon = async (req, res) => {
    try {

        //générer une rareté random
        randomRarity = MonsterLogic.getRarityByStones(req).id

        //récupérer tout les monstre de la rareté généré
        const originalMonster = await OriginalMonster.findAll({
            where: {
                rarityId: randomRarity
            }
        });
        
        // erreur si aucun monstre trouvé
        if (!originalMonster) {
            res.status(401).json({ error: "Monstre originel non trouvé."});
        }

        // récupérer un monstre aléatoirement parmis les monstre de la rareté
        let summonedMonster = originalMonster[Math.floor(Math.random() * originalMonster.length)].dataValues.id

        // Créer le monstre (linker sans la table monstre l'utilisateur et le monstre originel)
        const newMonster = await Monster.create({
            userId: req.session.userId,
            originalMonsterId: summonedMonster
        });

        // retourner le monstre
        res.status(200).json(newMonster);
    } catch (error) {
        res.status(400).json({ error: "erreur lors de l'invocation : " + error.message });
    }
};

exports.summonTenTime = async (req, res) => {
    try {

        let summonedMonster = []

        //génération des 10 monstre originel
        for(let i = 0; i < 10; i++){
                
            //générer une raretés random
            randomRarity = MonsterLogic.getRarityByStones(req).id

            //récupérer tout les monstre de la rareté généré
            const originalMonster = await OriginalMonster.findAll({
                where: {
                    rarityId: randomRarity
                }
            });
            
            // erreur si aucun monstre trouvé
            if (!originalMonster) {
                return res.status(401).json({ error: "Monstre originel non trouvé."});
            }

            // récupérer un monstre aléatoirement parmis les monstre de la rareté dans un tableau
            summonedMonster.push(originalMonster[Math.floor(Math.random() * originalMonster.length)].dataValues.id)

        }

        // tableau des nouveau monstre pour retourner la réponse
        let newMonsters = []

        // Créer les 10 monstre (linker sans la table monstre l'utilisateur et le monstre originel)
        for(let originalMob of summonedMonster) {
            
            newMonsters.push(
                await Monster.create({
                    userId: req.session.userId,
                    originalMonsterId: originalMob
                })
            )

        }

        // retourner le monstre
        res.status(200).json(newMonsters);
    } catch (error) {
        res.status(400).json({ error: "erreur lors de l'invocation : " + error.message });
    }
};

exports.getMonster = async (req, res) => {
    try {
        const monsterIds = req.body.MonstersIds;
        const userId = req.session.userId;  // Récupère l'ID de l'utilisateur à partir de la session

        // Rechercher le monstre par ID et vérifier qu'il appartient à l'utilisateur
        const monster = await Monster.findAll({
            where: {
                id: {
                    [Op.in]: monsterIds // Récupérer tous les monstres dont l'ID est dans la liste
                },
                userId: userId  // Assurez-vous que le monstre appartient bien à l'utilisateur
            },
            attributes: ['id', 'userId'], // sélectionner les champs voulu
            include: [
                {
                    model: OriginalMonster,
                    as: 'originalMonster',
                    attributes: ['id', 'name', 'base_hp', 'base_attack', 'base_defense', 'base_speed', 'base_crit_rate', 'base_crit_damage', 'base_resistance', 'base_accuracy', 'blocking', 'dodge', 'passive_care', 'sure_shot'],
                    include: [
                        { model: Type, as: 'type', attributes: ['id', 'name'] },
                        { model: Rarity, as: 'rarity', attributes: ['id', 'name'] },
                        { model: Role, as: 'role', attributes: ['id', 'name'] }
                    ]
                }
            ]
        });

        if (!monster) {
            return res.status(404).json({ message: 'Monster not found' });
        }
        res.status(200).json(monster);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
}


// Autres méthodes comme getMonsters, updateMonster, etc.