const Rune = require("../models/rune.model");


exports.createRune = async (runeData) => {
    try {
        if (runeData.slot < 1 || runeData.slot > 6) {
            throw new Error('Le numéro de l\'emplacement de la rune doit être compris entre 1 et 6.');
        }

        // Créer la rune
        const newRune = await Rune.create(runeData);
        console.log('Rune créée avec succès.');
        return newRune;
    } catch (error) {
        console.error('Erreur lors de la création de la rune:', error.message);
        throw error; // Re-lancer l'erreur pour la gestion par l'appelant
    }
};

exports.assignRuneToMonster = async (runeId, monsterId, slot) => {
    try {
        if (slot < 1 || slot > 6) {
            throw new Error('Le numéro de l\'emplacement de la rune doit être compris entre 1 et 6.');
        }

        // Vérifier si l'emplacement est déjà occupé pour le monstre
        const existingRune = await Rune.findOne({
            where: {
                monsterId: monsterId,
                slot: slot
            }
        });

        if (existingRune) {
            throw new Error('L\'emplacement de rune est déjà occupé.');
        }

        // Trouver la rune à associer
        const rune = await Rune.findByPk(runeId);
        if (!rune) {
            throw new Error('Rune non trouvée.');
        }

        // Associer la rune au monstre
        await rune.update({ monsterId: monsterId, slot: slot });

        console.log('Rune associée au monstre avec succès.');
    } catch (error) {
        console.error('Erreur lors de l\'association de la rune:', error.message);
        throw error; // Re-lancer l'erreur pour la gestion par l'appelant
    }
};