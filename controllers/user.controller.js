const User = require('../models/user.model');
const Consumable = require('../models/consumable.model');
const Currency = require('../models/currency.model');
const Monster = require('../models/monster.model');
const OriginalMonster = require('../models/originalMonster.model');
const Type = require('../models/type.model');
const Rarity = require('../models/rarity.model');
const Role = require('../models/role.model');

// Récupérer un utilisateur avec ses consommables et devises
exports.getUserWithDetails = async (req, res) => {
    try {
        const user = await User.findByPk(req.session.userId, {
            attributes: ['id', 'username'],
            include: [
                { model: Consumable, as: 'consumable', attributes: ['basic_stones', 'quality_stones', 'scarlet_stones'] },
                { model: Currency, as: 'currency', attributes: ['golds', 'gems'] },
                { model: Monster, as: "monsters", attributes: ['id'], include: [
                    {
                        model: OriginalMonster,
                        as: 'originalMonster',
                        attributes: ['id', 'name', 'base_hp', 'base_attack', 'base_defense', 'base_speed', 'base_crit_rate', 'base_crit_damage', 'base_resistance', 'base_accuracy', 'blocking', 'dodge', 'passive_care', 'sure_shot'],
                        include: [
                            { model: Type, as: 'type', attributes: ['id', 'name'] },
                            { model: Rarity, as: 'rarity', attributes: ['id', 'name'] },
                            { model: Role, as: 'role', attributes: ['id', 'name'] }
                        ]
                    }
                ]}
            ]
        });
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }
        res.json(user);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};
