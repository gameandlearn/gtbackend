const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Rune = sequelize.define('Rune', {
    master_stat: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    first_substat: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    second_substat: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    third_substat: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    fourth_substat: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    slot: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    monsterId: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
    // Ajoute d'autres champs selon les besoins
});

module.exports = Rune;