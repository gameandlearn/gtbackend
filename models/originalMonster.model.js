const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const OriginalMonster = sequelize.define('OriginalMonster', {
    family: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    base_hp: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_attack: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_defense: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_speed: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_crit_rate: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_crit_damage: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_resistance: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    base_accuracy: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    blocking: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    dodge: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    passive_care: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    sure_shot: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
});

module.exports = OriginalMonster;
