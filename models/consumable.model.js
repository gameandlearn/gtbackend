const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const User = require('./user.model');

const Consumable = sequelize.define('Consumable', {
    basic_stones: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    quality_stones: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    scarlet_stones: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
    // Ajoute d'autres champs selon les besoins
});

module.exports = Consumable;