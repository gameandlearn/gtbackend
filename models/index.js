const sequelize = require('../config/database');
const { applyAssociations } = require('./actions/applyAssociation');
const insertStaticData = require('./actions/insertStaticData');
const { syncModels } = require('./actions/syncModels');

// Définir les associations
applyAssociations()


// Synchroniser les tables / créer ou mettre à jour
syncModels()
    .then(() => {
        // Insérer les données statiques après la synchronisation
        insertStaticData();
    })
    .then(() => {
        console.log('Database setup complete.');
    })
    .catch(err => {
        console.error('Error setting up the database:', err);
    });

// Exporter les modèles et l'instance sequelize
module.exports = {
    sequelize,
};