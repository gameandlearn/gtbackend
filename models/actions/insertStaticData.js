const Rarity = require('../rarity.model');
const Type = require('../type.model');
const Role = require('../role.model');
const OriginalMonster = require('../originalMonster.model');

module.exports = function insertStaticData() {

    try {
        
        // Insérer les données pour Rarity
        Rarity.bulkCreate([
            { id: 1, name: 'common' },
            { id: 2, name: 'rare' },
            { id: 3, name: 'epic' },
            { id: 4, name: 'legendary' },
            { id: 5, name: 'mythic' }
        ], { ignoreDuplicates: true });

        // Insérer les données pour Type
        Type.bulkCreate([
            { id: 1, name: 'fire' },
            { id: 2, name: 'water' },
            { id: 3, name: 'Foudre' },
            { id: 4, name: 'lightning' },
            { id: 5, name: 'light' },
            { id: 6, name: 'darkness' }
        ], { ignoreDuplicates: true });

        // Insérer les données pour Role
        Role.bulkCreate([
            { id: 1, name: 'attacker' },
            { id: 2, name: 'tank' },
            { id: 3, name: 'support' },
            { id: 4, name: 'assassin' }
        ], { ignoreDuplicates: true });

        // Insérer les données pour OriginalMonster (exemple)
        OriginalMonster.bulkCreate([
            // blob
            {   family: 'blob', name: 'blobC', base_hp: 500, base_attack: 50, base_defense: 40, base_speed: 90, base_crit_rate: 15, base_crit_damage: 50, base_resistance: 0, base_accuracy: 20, rarityId: 1, typeId: 1, roleId: 1  },
            {   family: 'blob', name: 'blobR', base_hp: 500, base_attack: 50, base_defense: 40, base_speed: 90, base_crit_rate: 15, base_crit_damage: 50, base_resistance: 0, base_accuracy: 20, rarityId: 2, typeId: 1, roleId: 1  },
            {   family: 'blob', name: 'blobE', base_hp: 500, base_attack: 50, base_defense: 40, base_speed: 90, base_crit_rate: 15, base_crit_damage: 50, base_resistance: 0, base_accuracy: 20, rarityId: 3, typeId: 1, roleId: 1  },
            {   family: 'blob', name: 'blobL', base_hp: 500, base_attack: 50, base_defense: 40, base_speed: 90, base_crit_rate: 15, base_crit_damage: 50, base_resistance: 0, base_accuracy: 20, rarityId: 4, typeId: 1, roleId: 1  },
            {   family: 'blob', name: 'blobM', base_hp: 500, base_attack: 50, base_defense: 40, base_speed: 90, base_crit_rate: 15, base_crit_damage: 50, base_resistance: 0, base_accuracy: 20, rarityId: 5, typeId: 1, roleId: 1  },
            // Autres monstres
        ], { ignoreDuplicates: true });

        console.log("static data added !")
        
    } catch (error) {
        console.log("error adding static data", error)
    }
};