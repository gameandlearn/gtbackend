const Consumable = require("../consumable.model");
const Currency = require("../currency.model");
const Monster = require("../monster.model");
const OriginalMonster = require("../originalMonster.model");
const Rarity = require("../rarity.model");
const Role = require("../role.model");
const Rune = require("../rune.model");
const Type = require("../type.model");
const User = require("../user.model");

// permet de définir un ordre logique d'actualisation des tables
exports.syncModels = async () => {
    try {
        await User.sync({alter: true})

        await Currency.sync({alter: true})
        await Consumable.sync({alter: true})
        
        await Rarity.sync({alter: true})
        await Type.sync({alter: true})
        await Role.sync({alter: true})

        await OriginalMonster.sync({alter: true})

        await Monster.sync({alter: true})
        
        await Rune.sync({alter: true})

        console.log('Tables synchronized successfully in order.');
        
    } catch (error) {
        console.error('Error synchronizing tables:', error);
    }
}