const User = require('../user.model');
const Monster = require('../monster.model');
const Currency = require('../currency.model');
const Consumable = require('../consumable.model');
const Rune = require('../rune.model');
const Rarity = require('../rarity.model');
const OriginalMonster = require('../originalMonster.model');
const Type = require('../type.model');
const Role = require('../role.model');

exports.applyAssociations = () => {
    
    User.hasMany(Monster, { foreignKey: { name: 'userId', allowNull: false }, as: 'monsters' });
    Monster.belongsTo(User, { foreignKey: { name: 'userId', allowNull: false }, as: 'user' });

    User.hasOne(Currency, { foreignKey: { name: 'userId', allowNull: false }, as: 'currency' });
    Currency.belongsTo(User, { foreignKey: { name: 'userId', allowNull: false }, as: 'user' })

    User.hasOne(Consumable, { foreignKey: { name: 'userId', allowNull: false }, as: 'consumable' });
    Consumable.belongsTo(User, { foreignKey: { name: 'userId', allowNull: false }, as: 'user' })
    
    User.hasMany(Rune, {foreignKey: { name: 'userId', allowNull: false }, as: "runes"})
    Rune.belongsTo(User, { foreignKey: { name: 'userId', allowNull: false }, as: 'user' })
    
    Monster.hasMany(Rune, {foreignKey: "monsterId", as: "runes"})
    Rune.belongsTo(Monster, { foreignKey: 'monsterId', as: 'monster' })

    Rarity.hasMany(OriginalMonster, { foreignKey: { name: 'rarityId', allowNull: false }, as: 'originalMonsters' });
    OriginalMonster.belongsTo(Rarity, { foreignKey: { name: 'rarityId', allowNull: false }, as: 'rarity' });

    Type.hasMany(OriginalMonster, { foreignKey: { name: 'typeId', allowNull: false }, as: 'originalMonsters' });
    OriginalMonster.belongsTo(Type, { foreignKey: { name: 'typeId', allowNull: false }, as: 'type' });

    Role.hasMany(OriginalMonster, { foreignKey: { name: 'roleId', allowNull: false }, as: 'originalMonsters' });
    OriginalMonster.belongsTo(Role, { foreignKey: { name: 'roleId', allowNull: false }, as: 'role' });

    OriginalMonster.hasMany(Monster, { foreignKey: { name: 'originalMonsterId', allowNull: false }, as: 'monster' });
    Monster.belongsTo(OriginalMonster, { foreignKey: { name: 'originalMonsterId', allowNull: false }, as: 'originalMonster' });

};