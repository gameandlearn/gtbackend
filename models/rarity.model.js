const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Rarity = sequelize.define('Rarity', {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    }
});

module.exports = Rarity;
