const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const User = require('./user.model');

const Currency = sequelize.define('Currency', {
    golds: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    gems: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
    // Ajoute d'autres champs selon les besoins
});

module.exports = Currency;