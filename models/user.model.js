const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const bcrypt = require('bcrypt');
const Currency = require('./currency.model');
const Consumable = require('./consumable.model');

const User = sequelize.define('User', {
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    }
});

// Méthode pour hacher le mot de passe avant de sauvegarder l'utilisateur
User.beforeCreate(async (user) => {
    user.password = await bcrypt.hash(user.password, 10);
});

User.afterCreate(async (user, option) => {
    try {
        console.log(user.id);
        await Currency.create({userId: user.id, golds: 1000, gems: 500})
        await Consumable.create({userId: user.id, basic_stones: 10})
    } catch (error) {
        console.error('Error creating Currency and Consumable:', error);
        throw error;
    }
})

User.beforeDestroy(async (user) => {
    try {
        await Currency.destroy({ where: { userId: user.id } });
        await Consumable.destroy({ where: { userId: user.id } });
    } catch (error) {
        console.error('Error deleting Currency and Consumable:', error);
    }
});

module.exports = User;
