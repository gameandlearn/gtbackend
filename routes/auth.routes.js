const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth.controller');
const authM = require('../middleware/auth.middleware');

// Routes d'authentification
router.post('/register', authController.register);
router.post('/login', authController.login);
router.get('/session/:id', authM.isAuthenticated, authController.loginIfSessionActive);
router.post('/logout', authController.logout);

module.exports = router;
