const express = require('express');
const router = express.Router();
const monsterController = require('../controllers/monster.controller');
const authM = require('../middleware/auth.middleware');
const consumableM = require('../middleware/consumable.middleware');


router.post('/monsters/summonTenTime', authM.isAuthenticated, consumableM.allowSummonTenTime, monsterController.summonTenTime);
router.post('/monsters/summon', authM.isAuthenticated, consumableM.allowSummon, monsterController.summon);
router.get('/monsters/get', authM.isAuthenticated, monsterController.getMonster);
// Autres routes...

module.exports = router;