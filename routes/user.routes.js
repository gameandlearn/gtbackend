const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user.controller');
const authM = require('../middleware/auth.middleware');

// Route pour récupérer un utilisateur avec ses consommables et devises
router.get('/user/details',authM.isAuthenticated, UserController.getUserWithDetails);

module.exports = router;