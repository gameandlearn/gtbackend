const authM = {}

// Middleware pour vérifier si l'utilisateur est authentifié
authM.isAuthenticated = (req, res, next) => {
    if (req.session && req.session.userId) {
        // Utilisateur authentifié, passer au middleware suivant
        return next();
    } else {
        // Utilisateur non authentifié, envoyer une réponse d'erreur
        return res.status(401).json({ message: 'You need to log in first.' });
    }
};

module.exports = authM