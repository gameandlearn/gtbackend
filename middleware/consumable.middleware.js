const Consumable = require("../models/consumable.model");

const consumableM = {}

const getConsumable = async (req) => {

    return Consumable.findOne({
        where: {
            userId: req.session.userId
        }
    })

};

// Middleware pour vérifier si l'utilisateur peut summon avec des consommable
consumableM.allowSummon = async (req, res, next) => {

    //récupérer les consommable de l'utilisateur
    const userConsumables = await getConsumable(req)
    //récupérer le nombre de consommable demandé dans la requète
    const usedConsumable = userConsumables.dataValues[req.body.stone]

    if (usedConsumable >= 1) {
        
        // retire le consommable une fois la requète suivant le middleware soit éxécuté avec succès
        res.on("finish", () => {

            if (res.statusCode >= 200 && res.statusCode < 300) {
                //retirer 1 consommable utilisé, définis par la requête
                Consumable.update(
                    {
                        [req.body.stone]: usedConsumable - 1
                    },
                    {
                        where: {
                            userId: req.session.userId
                        }
                    }
                );
            }
            
        })
        return next();
    } else {
        // l'utilisateur n'a pas assez de pierre pour invoquer
        return res.status(401).json({ message: 'Not enough stone', code: "not_enough_stone" });
    }
};

// Middleware pour vérifier si l'utilisateur peut summon 10 fois avec des consommable
consumableM.allowSummonTenTime = async (req, res, next) => {


    //récupérer les consommable de l'utilisateur
    const userConsumables = await getConsumable(req)
    //récupérer le nombre de consommable demandé dans la requète
    const usedConsumable = userConsumables.dataValues[req.body.stone]

    if (usedConsumable >= 10) {
        
        // retire le consommable une fois la requète suivant le middleware soit éxécuté avec succès
        res.on("finish", () => {

            if (res.statusCode >= 200 && res.statusCode < 300) {
                //retirer 1 consommable utilisé, définis par la requête si requète à succès
                Consumable.update(
                    {
                        [req.body.stone]: usedConsumable - 10
                    },
                    {
                        where: {
                            userId: req.session.userId
                        }
                    }
                );
            }
            
        })
        return next();
    } else {
        // l'utilisateur n'a pas assez de pierre pour invoquer
        return res.status(401).json({ message: 'Not enough stone', code: "not_enough_stone" });
    }
};

module.exports = consumableM